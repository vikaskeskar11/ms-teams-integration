const logger = require('./logger/logger')
const msTeamsService = require('./services/ms-teams-service')
const teamCreationPayload = {
  authorization: '',
  teamName: '',
  teamMembers: [{
    userId: 'vkesakar@gmail.com',
    role: 'admin'
  },
  {
    userId: 'dummy_1@gmail.com',
    role: 'admin'
  }]
}

class Index {
  async invoke () {
    try {
      const { message } = await msTeamsService.createTeam(teamCreationPayload)
      logger.info('Index:invoke: Team created ', { message })
    } catch (error) {
      logger.error('Index:invoke: Error during team creation ', { error })
    }
  }
}

new Index()
  .invoke()
